<?php

namespace App\Controller;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\Interfaces\RecordControllerInterfaces;
use App\Security\JsonWebTokenUser;
use App\Validator\RecordValidator;
use App\Http\Requests\PostRecordRequest;
use App\Http\Requests\PutRecordRequest;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\DataManager\Encoder;
use App\Repository\RecordRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Service\CitiesService;
/**
 *
 * @ApiResource()
 * Class RecordController
 * @package App\Controller
 */
class RecordController extends RecordControllerInterfaces
{

    /**
     * @var ValidatorInterface
     */
    private ValidatorInterface $validator;
    /**
     * @var Encoder
     */
    private Encoder $encoder;
    /**
     * @var RecordRepository
     */
    private RecordRepository $repository;
    /**
     * @var TokenStorageInterface
     */
    private TokenStorageInterface $storage;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;
    /**
     * @var CitiesService
     */
    private CitiesService $citiesService;


    public function __construct(RecordRepository $repository,
                                Encoder $encoder,
                                TokenStorageInterface $storage,
                                SerializerInterface $serializer,
                                ValidatorInterface $validator,
                                CitiesService $citiesService)
    {
        $this->storage = $storage;
        $this ->encoder = $encoder;
        $this->repository = $repository;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->citiesService = $citiesService;

    }

    /**
     * @Route("/api/record",
     *     name="getAll",
     *     methods={"GET"})
     * @OA\Get(
     *     summary="Получить все записи",
     *     tags={"Basic"})
     */
    public function getAll(Request $request): Response
    {
        try {
            $params = $request->query->all();
            $params = $this->paramsDecode($params);
            $data = $this->repository->findByProgress($params['filter'],$params['sort'],$params['limit'],$params['offset']);
            return new Response($this->encoder->toJSON($data));
        }
        catch (Exception $err){
            return new Response($err->getMessage(),400);
        }
    }
    /**
     * @Route("/api/record/cities",
     *     name="getAllCities",
     *     methods={"GET"})
     * @OA\Get(
     *     summary="Получить все активные города",
     *     tags={"Basic"})
     */
    public function getAllCities(): Response
    {

        try {
            $list = $this->citiesService->getList(true);
            return new Response($this->encoder->toJSON(['total'=>count($list),'items'=>$list]));
        }
        catch (Exception $err){
            return new Response($err->getMessage(),400);
        }
    }

    /**
     * @Route("/api/record/{id}",name="getByID",methods ={"GET"})
     * @param Request $request
     * @return Response
     * @OA\Get(
     *     summary="Получить запись по ID",
     *     tags={"Basic"})
     */
    public function getByID(Request $request): Response
    {
        try {
            $id = $request->get('id');
            $this->revalidator->idValidation($id);
            $categoryFields = $this->repository->findOneBy(array('id' => $id));
            return new Response($this->encoder->toJSON($categoryFields));
        } catch (Exception $err) {
            return new Response($err->getMessage(),400);
        }
    }
    /**
     * @Route("/api/record",name="post",methods={"POST"})
     * @param Request $request
     * @return Response
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *          type="object",
     *          ref=@Model(type=App\Http\Requests\PostRecordRequest::class)
     *     )
     *  )
     * @OA\Post(
     *     summary="Добавить новую запись",
     *     tags={"Basic"})
     */
    public function post(Request $request): Response
    {
        try {
            $content = $request->getContent();
            $postRecord = $this->serializer->deserialize($content, PostRecordRequest::class, "json");
            if ($this->validator->validate($postRecord)) {
                try {
                    $token = $this->storage->getToken();
                    if ($token instanceof TokenInterface) {
                        /** @var UserInterface $user */
                        $user = $token->getUser();
                        if ($user instanceof JsonWebTokenUser) {
                            $postRecord->setAuthorCreate($user->getVisitorId());
                            $postRecord->setEmail($user->getEmail());
                        }
                    }
                    $result = $this->repository->post($postRecord);
                    return new Response($result[0], $result[1]);
                } catch (Exception $err) {
                    return new Response($err->getMessage(), 400);
                }
            }
        } catch (Exception $err) {
            return new Response($err->getMessage(), 400);
        }
    }
    /**
     * @Route("/api/record/{id}",name="put",methods={"PUT"})
     * @param Request $request
     * @return Response
     * @OA\RequestBody(
     *     @OA\JsonContent(
     *        type="object",
     *          ref=@Model(type=App\Http\Requests\PutRecordRequest::class)
     *     )
     * )
     * @OA\Put(
     *     summary="Обновить поля Записи по указанному ID",
     *     tags={"Basic"})
     */
    public function put(Request $request): Response
    {
        try {
            $id = $request->get('id');
            $content = $request->getContent();
            $putRecord = $this->serializer->deserialize($content, PutRecordRequest::class, "json");
            if ($this->validator->validate($putRecord)) {
                try {
                    $token = $this->storage->getToken();
                    if ($token instanceof TokenInterface) {
                        /** @var UserInterface $user */
                        $user = $token->getUser();
                        if ($user instanceof JsonWebTokenUser) {
                            $putRecord->setAuthorChange($user->getVisitorId());
                        }
                    }
                    $result = $this->repository->put($id, $putRecord);
                    return new Response($result[0], $result[1]);
                } catch (Exception $err) {
                    return new Response($err->getMessage(), 400);
                }
            }
            else{
                return new Response('Отправленные данные содержат ошибку', 400);
            }
        } catch (Exception $err) {
            return new Response($err->getMessage(), 400);
        }
    }
    /**
     * @Route("/api/record/{id}",name="delete",methods={"DELETE"})
     * @param Request $request
     * @return Response
     * @OA\Delete(
     *     summary="Удаляет запись по ID",
     *     tags={"Basic"})
     */
    public function delete(Request $request): Response
    {
        try {
           $id = $request->get('id');
           $this->revalidator->idValidation($id);
           $result = $this->repository->delete($id);

            return new Response($result[0], $result[1]);
        } catch (Exception $err) {
            return new Response($err->getMessage(),400);
        }
    }



}
