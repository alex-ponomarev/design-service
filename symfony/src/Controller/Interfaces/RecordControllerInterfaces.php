<?php
namespace App\Controller\Interfaces;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RecordControllerInterfaces extends AbstractController
{
    public function paramsDecode($params){
        if(!isset($params['limit'])){
            $params['limit'] = 30;
        }
        if(!isset($params['offset'])){
            $params['offset'] = 0;
        }
        if(!isset($params['sort'])){
            $params['sort'] = ['id'=>'DESC'];
        } else {
            $params['sort'] = (array)json_decode($params['sort']);
            if(count($params['sort'])===0){
                $params['sort'] = ['id' => 'DESC'];
            }
        }
        if(!isset($params['filter'])){
            $params['filter'] = array();
        }else {
            $params['filter'] = (array)json_decode($params['filter']);
            if(count($params['filter'])===0){
                $params['filter'] = array();
            };
        }
        return $params;
    }
}