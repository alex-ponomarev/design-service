<?php


namespace App\Security;

use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;

class JsonWebTokenUser implements JWTUserInterface, \Stringable
{
    private string $username;
    private int $id = 0;
    /** @var string[] */
    private array $roles;
    private string $issuer;
    private string $token;
    private int $subject;
    private array $userGroups;
    private int $visitorId;
    private int $customerId;
    private string $email;
    private string $service;
    private string $priceCategory = "R";

    private function __construct(
        string $username,
        string $email,
        int $visitorId,
        string $service,
        array $groups,
        int $customerId,
        array $roles,
        int $subject,
        string $issuer)
    {
        $this->username = $username;
        $this->email = $email;
        $this->visitorId = $visitorId;
        $this->service = $service;
        $this->userGroups = $groups;
        $this->customerId = $customerId;
        $this->roles = $roles;
        $this->subject = $subject;
        $this->issuer = $issuer;

        if (!empty($this->subject)) {
            $this->id = (int)$this->subject;
        }
    }

    public static function createFromPayload($username, array $payload)
    {
        $email = $payload["email"] ?? "";
        $visitorId = $payload["visitorId"];
        $service = $payload["service"];
        $groups = $payload["userGroups"];
        $customerId = $payload["baseUserId"];
        $roles = $payload["roles"];
        $subject = $payload["sub"];
        $issuer = $payload["iss"];


        return new self((string)$username,
            (string)$email,
            (int)$visitorId,
            (string)$service,
            (array)$groups,
            (int)$customerId,
            (array)$roles,
            (int)$subject,
            (string)$issuer
        );
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getUsername()
    {
        return $this->username;
    }
    public function getCustomerId()
    {
        return $this->customerId;
    }
    public function getVisitorId()
    {
        return $this->visitorId;
    }
    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return '4';
    }

    public function getSalt()
    {
        return '4';
    }

    public function eraseCredentials()
    {
        return [4];
    }

    public function __toString()
    {
        return "";
    }
}