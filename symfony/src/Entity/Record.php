<?php

namespace App\Entity;

use App\Repository\RecordRepository;
use Doctrine\ORM\Mapping as ORM;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="Record")
 * @ORM\Entity(repositoryClass=RecordRepository::class)
 */
class Record
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"record:get","record:post","record:put"})
     */
    private $dateReceipt;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"record:get", "record:put"})
     */
    private $active;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"record:get","record:post","record:put"})
     */
    private $sort;

    /**
     * @ORM\Column(type="integer")
     * @OA\Property(readOnly="true")
     * @Groups({"record:get"})
     */
    private $authorCreate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @OA\Property(readOnly="true")
     * @Groups({"record:get"})
     */
    private $authorChange;

    /**
     * @ORM\Column(type="datetime")
     * @OA\Property(readOnly="true")
     * @Groups({"record:get"})
     */
    private $dateCreate;

    /**
     * @OA\Property(readOnly="true")
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"record:get"})
     */
    private $dateChange;
    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"record:get","record:post","record:put"})
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @OA\Property(readOnly="true")
     * @Groups({"record:get"})
     */
    private $email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateReceipt()
    {
        return $this->dateReceipt;
    }

    public function setDateReceipt($dateReceipt): self
    {
        $this->dateReceipt = $dateReceipt;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(?int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }
    public function getAuthorCreate(): ?int
    {
        return $this->authorCreate;
    }

    public function setAuthorCreate(int $authorCreate): self
    {
        $this->authorCreate = $authorCreate;

        return $this;
    }

    public function getAuthorChange(): ?int
    {
        return $this->authorChange;
    }

    public function setAuthorChange(?int $authorChange): self
    {
        $this->authorChange = $authorChange;

        return $this;
    }
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    public function setDateCreate($dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }
    public function getDateChange()
    {
        return $this->dateChange;
    }

    public function setDateChange($dateChange): self
    {
        $this->dateChange = $dateChange;

        return $this;
    }

    public function getRegion(): ?int
    {
        return $this->region;
    }

    public function setRegion(?int $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

}
