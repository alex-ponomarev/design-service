<?php

namespace App\Repository;

use App\Entity\Record;
use App\Http\Requests\PostRecordRequest;
use App\Http\Requests\PutRecordRequest;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Validator\RecordValidator;

/**
 * @method Record|null find($id, $lockMode = null, $lockVersion = null)
 * @method Record|null findOneBy(array $criteria, array $orderBy = null)
 * @method Record[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecordRepository extends ServiceEntityRepository
{
    /**
     * @var RecordValidator
     */
    private RecordValidator $validator;

    public function __construct(ManagerRegistry $registry, RecordValidator $validator)
    {
        $this ->validator = $validator;
        parent::__construct($registry, Record::class);
    }
    public function findByProgress(array $criteria = null, $orderBy = ['id' => 'DESC'], $limit = null, $offset = null){
        $items = $this->findBy($criteria,$orderBy,$limit,$offset);
        $total = $this->elementsCount($criteria);

        return ['total'=>$total,'items'=>$items];
    }

    public function elementsCount(array $criteria = null){
        $qb = $this->createQueryBuilder('r');
        if($criteria != null) {
            foreach ($criteria as $field => $val) {
                $op = "=";
                $or = [];
                if (is_array($val) && isset($val['op'])) {
                    $op = $val['op'];
                    $val = $val['val'];
                } elseif (is_array($val)) {
                    $or = [];
                    foreach ($val as $ind => $_val) {
                        $or[] = "r.{$field} {$op} :{$field}_{$ind}";
                        $qb->setParameter("{$field}_{$ind}", $_val);
                    }
                }

                if ($or) {
                    $qb->andWhere(implode(" or ", $or));
                } else {
                    $qb->andWhere("r.{$field} {$op} :{$field}")
                        ->setParameter($field, $val);
                }

            }
        }
        return  $qb
            ->select('count(r.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

        public function post(PostRecordRequest $postRecord)
    {
        $record = new Record();
        $record->setDateReceipt($postRecord->getDateReceipt());
        $record->setSort($postRecord->getSort());
        $record->setRegion($postRecord->getRegion());
        $record->setEmail($postRecord->getEmail());
        $record->setAuthorCreate($postRecord->getAuthorCreate());
        $record->setDateCreate(new DateTime());
        $record->setActive(true);

        $this->_em->persist($record);
        $this->_em->flush();
        return ['Новая запись внесена и существует под id = '.$record->getId(), 200];


    }
    public function put($id,PutRecordRequest $putRecord)
    {
        $record = $this->findOneBy(array('id' => $id));
        $record->setDateReceipt($putRecord->getDateReceipt());
        $record->setActive($putRecord->getActive());
        $record->setSort($putRecord->getSort());
        $record->setRegion($putRecord->getRegion());
        $record->setAuthorChange($putRecord->getAuthorChange());
        $record->setDateChange(new DateTime());

        $this->_em->persist($record);
        $this->_em->flush();
        return ['Запись '.$id.' успешно обновлена', 200];

    }
    public function delete($id)
    {
        $record = $this->findOneBy(array('id' => $id));
        $this->_em->remove($record);
        $this->_em->flush();
        return ['Запись id='.$id.'удалена', 200];
    }
/*
    public function fileProductsPost($productFields)
    {
        foreach ($productFields as $fields) {
            $record = new Record();
            $existsFields = ['Название','Активность','Сортировка','Дата изменения'];
            if(count($existsFields)>0) {
                $datetime = '';
                foreach ($existsFields as $field) {
                    switch ($field) {
                        case 'Название':
                            $record->setDateReceipt($fields['Название']);
                            break;
                        case 'Активность':
                            if($fields['Активность'] == 'Да')
                            $record->setActive(true);
                            else if($fields['Активность'] == 'Нет')
                            $record->setActive(false);
                            break;
                        case 'Сортировка':
                            $record->setSort($fields['Сортировка']);
                            break;
                        case 'Дата изменения':
                            $datetime = $fields['Дата изменения'];
                            break;
                        case 'ID':
                            $record->setAuthorCreate($fields['ID']);
                            break;
                    }
                }
                $record->setDateCreate(DateTime::createFromFormat('y.m.d G:i:s',$datetime));
            }

            $this->_em->persist($record);

        }
        $this->_em->flush();
    }

    public function checkDBConnection(): bool
    {
        return $this->_em->getConnection()->isConnected();
    }
*/
    // /**
    //  * @return Record[] Returns an array of Record objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Record
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
