<?php

namespace App\Service;

/**
 * Временно, пока не будет сервиса конфига городов
 * Class CitiesService
 * @package App\Service
 */
class CitiesService
{
	private static $arCity = array(
		'acity' => array(
			'NEW_SCHEME_ID' => '17', // следующий 18 и т.д.
			'ACTIVE' => "Y",
			'NAME' => 'Другой город',
			'NAME2' => 'Другом городе',
			'NAME3' => 'Другом городе',
			'ISO_REGION' => 'RU-MOS',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '501',
			'KLADR' => '7700000000000',
			'REGION_KLADR' => '77',
			'PRICE_ID' => 28,
			'PRICE_CODE' => 'ACITY',
			'QUANTITY_CODE' => 'Q_ACITY',
			'SHOW_IN_MENU' => 'Y',
			'EXTERNAL_SUPPLIERS' => array(  /* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'OPT_STORE_ID' => 501, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array( /*"89.109.236.75", "85.21.93.110" */),
			'DEFAULT_B2C' => 501, // для фидов
			'MAPS_COORDS' => array("55.722801", "37.674984"), // координаты города для карт
			'STORES' => array(
				'501' => array(
					'NEW_SCHEME_ID' => '501',
					'CITY_CODE' => 'acity',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Другой город',
					'MAIN' => false,
					'1C_CODE' => '501', // участвовало в какой-то старой логике
					'REAL_1C_STORE' => '50', // будет участвовать в новой схеме для подмены значения склада в обмене
					'TERMINAL_SHOW_PICKUP' => 'N',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => '',
					'ADRESS_SKLADA_WITHOUT_CITY' => '',
					'SHOW_PICKUP' => false,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Максимальная сумма гаказа при которой грузчики доступны
					'PROP_ID' => '2659', // ID свойства "Остаток" для данного города
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('55.722801, 37.674984'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'пн-вс с 10:00 до 22:00',
					'REZERV_DO' => '21:30', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("10:00-22:00"),
					'APPEAL_FORM_CODE' => 1,
					'VACANCY_FORM_CODE' => 1,
					'CRM_FORM_CODE' => 1, // как оказалось, во всех формах crm одинаковые коды для городов.
				),
			),
			'EXTERNAL_STORES' => array(
				'chehov' => array(
					'STORES' => array(
						53
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						4,//454,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => false,
			"FIRST_FREE_DELIVERY" => false, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '9999999999999999',
			'FREE_DELIVERY_PRICE_M' => '9999999999999999',
			'FREE_DELIVERY_PRICE_B2B' => '9999999999999999',
			'LOAD_SUM_SKLAD' => '0.2', // Стоимость подъема одного килограма на этаж - сейчас не используется
			'DELIVERY_X_HOUR' => '20', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '333789',
			'CALL_CENTER_PHONE_NUM' => '+7 (495) 122-20-20',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (495) 122-20-20',
			'LIGHT_STORE' => false,
			'CITIES' => [
				['NAME' => 'Архангельск', 'MAPS_COORDS' => ["64.539393", "40.516939"]],
				['NAME' => 'Астрахань', 'MAPS_COORDS' => ["46.347869", "48.033574 "]],
				['NAME' => 'Белгород', 'MAPS_COORDS' => ["50.595660", "36.587223"]],
				['NAME' => 'Биробиджан', 'MAPS_COORDS' => ["48.792947", "132.920245"]],
				['NAME' => 'Благовещенск', 'MAPS_COORDS' => ["50.290640", "127.527173"]],
				['NAME' => 'Брянск', 'MAPS_COORDS' => ["53.243562", "34.363407"]],
				['NAME' => 'Великий Новгород', 'MAPS_COORDS' => ["58.522810", "31.269915"]],
				['NAME' => 'Волгоград', 'MAPS_COORDS' => ["48.707073", "44.516930"]],
				['NAME' => 'Воронеж', 'MAPS_COORDS' => ["51.660781", "39.200269"]],
				['NAME' => 'Екатеринбург', 'MAPS_COORDS' => ["56.838011", "60.597465"]],
				['NAME' => 'Иркутск', 'MAPS_COORDS' => ["52.287054", "104.281047"]],
				['NAME' => 'Калининград', 'MAPS_COORDS' => ["54.707390", "20.507307"]],
				['NAME' => 'Кемерово', 'MAPS_COORDS' => ["55.354727", "86.088374"]],
				['NAME' => 'Киров', 'MAPS_COORDS' => ["58.603591", "49.668014"]],
				['NAME' => 'Курган', 'MAPS_COORDS' => ["55.441004", "65.341118"]],
				['NAME' => 'Курск', 'MAPS_COORDS' => ["51.730361", "36.192647"]],
				['NAME' => 'Липецк', 'MAPS_COORDS' => ["52.608820", "39.599220"]],
				['NAME' => 'Магадан', 'MAPS_COORDS' => ["59.568164", "150.808541"]],
				['NAME' => 'Мурманск', 'MAPS_COORDS' => ["68.970682", "33.074981"]],
				['NAME' => 'Новосибирск', 'MAPS_COORDS' => ["55.030199", "82.920430"]],
				['NAME' => 'Омск', 'MAPS_COORDS' => ["54.989342", "73.368212"]],
				['NAME' => 'Орёл', 'MAPS_COORDS' => ["52.970371", "36.063837"]],
				['NAME' => 'Оренбург', 'MAPS_COORDS' => ["51.768199", "55.096955"]],
				['NAME' => 'Пенза', 'MAPS_COORDS' => ["53.195063", "45.018316"]],
				['NAME' => 'Псков', 'MAPS_COORDS' => ["57.819250", "28.332065"]],
				['NAME' => 'Ростов-на-Дону', 'MAPS_COORDS' => ["47.222078", "39.720349"]],
				['NAME' => 'Рязань', 'MAPS_COORDS' => ["54.629216", "39.736375"]],
				['NAME' => 'Самара', 'MAPS_COORDS' => ["53.195538", "50.101783"]],
				['NAME' => 'Санкт-Петербург', 'MAPS_COORDS' => ["59.939095", "30.315868"]],
				['NAME' => 'Саратов', 'MAPS_COORDS' => ["51.533103", "46.034158"]],
				['NAME' => 'Тверь', 'MAPS_COORDS' => ["56.859847", "35.911995"]],
				['NAME' => 'Томск', 'MAPS_COORDS' => ["56.484640", "84.947649"]],
				['NAME' => 'Тула', 'MAPS_COORDS' => ["54.193122", "37.617348"]],
				['NAME' => 'Тюмень', 'MAPS_COORDS' => ["57.153033", "65.534328"]],
				['NAME' => 'Ульяновск', 'MAPS_COORDS' => ["54.314192", "48.403123"]],
				['NAME' => 'Челябинск', 'MAPS_COORDS' => ["55.159897", "61.402554"]],
				['NAME' => 'Южно-Сахалинск', 'MAPS_COORDS' => ["46.959155", "142.738023"]],
			],
			"RUSSIAN_POST" => true,
			"DPD" => true,
			"CDEK" => true,
			"EXTERNAL_DELIVERY" => true,
			'NO_SERVICES' => true,
		),
		/*s-city*/
		'aleksandrov' => array(
			'ACTIVE' => 'N',
			'NAME' => 'Александров',
			'NAME2' => 'Александрове',
			'NAME3' => 'Александрове',
			'ISO_REGION' => 'RU-VLA',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '39',
			'KLADR' => '3300000100000',
			'REGION_KLADR' => '33',
			'PRICE_ID' => 18,
			'PRICE_CODE' => 'ALEKSANDROV',
			'QUANTITY_CODE' => 'Q_ALEKSANDROV',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 39, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk',
				'tech', 'wh', 'esp', 'anr', 'realk'
			, 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14', 'orsi', 'oslt', 'luc', 'ask', 'krsh',
				'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains'),
			'TERMINALS' => array("91.195.131.188"),
			'DEFAULT_B2C' => 39, // для фидов
			'MAPS_COORDS' => array("56.391984194163484", "38.718268999999935"), // координаты города для карт
			'STORES' => array(
				'39' => array(
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Александров',
					'MAIN' => true,
					'1C_CODE' => '39',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Александров, Двориковское шоссе, 23',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'Двориковское шоссе, 23',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1327', // ID свойства "Остаток" для данного города '1159'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('56.402533', '38.690390'), // координаты склада (для оптовых менеджеров)
					'REZHIM_RABOTY' => 'с 10:00 до 18:00, понедельник - выходной день', //режим работы
					'REZERV_DO' => '18:30', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => false,
					'IS_EXT_AS_LOCAL' => true, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array(false, "10:00-18:00", "10:00-18:00", "10:00-18:00", "10:00-18:00", "10:00-18:00", "10:00-18:00"),

				),
			),
			'EXTERNAL_STORES' => array(
//				'yaroslavl' => array(
//					'STORES' => array(
//						4,
//					),
//				),
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '0',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array("Вичуга", "Родники"),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '373274',
			'CALL_CENTER_PHONE_NUM' => '8-800-100-4900',
			'LIGHT_STORE' => true,
		),
		/*s-city*/
		'vladimir' => array(
			'NEW_SCHEME_ID' => '3',
			'ACTIVE' => 'Y',
			'NAME' => 'Владимир',
			'NAME2' => 'Владимире',
			'NAME3' => 'Владимире',
			'ISO_REGION' => 'RU-VLA',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '36',
			'KLADR' => '3300000100000',
			'REGION_KLADR' => '33',
			'PRICE_ID' => 16,
			'PRICE_CODE' => 'VLADIMIR',
			'QUANTITY_CODE' => 'Q_VLADIMIR',
			'SHOW_IN_MENU' => 'Y',
			/*s-suppliers*/
			'EXTERNAL_SUPPLIERS' => array(  /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik', 'tech', 'akc',
				'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14', 'orsi',
				'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'), // Здесь перечисляем какие внешние поставщики работают в данном городе;p
			'OPT_STORE_ID' => 36, //35, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array("176.107.2.196", "95.66.165.146"),
			'DEFAULT_B2C' => 36, // для фидов
			'MAPS_COORDS' => array("56.138330175994874", "40.42147049999998"), // координаты города для карт
			'STORES' => array(
				'36' => array(
					'NEW_SCHEME_ID' => '36',
					'CITY_CODE' => 'vladimir',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Владимир',
					'MAIN' => true,
					'1C_CODE' => '36',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Владимир, ул.Ноябрьская, д.131',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул.Ноябрьская, д.131',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1159', // ID свойства "Остаток" для данного города '1159'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('56.0908', '40.2389'), // координаты склада (для оптовых менеджеров)
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00', //режим работы
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => true, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 11,
					'VACANCY_FORM_CODE' => 11,
					'CRM_FORM_CODE' => 11,
				),
				'35' => array(
					'CITY_CODE' => 'vladimir',
					'VISIBILITY' => 0, // отображение склада на сайте
					'ACTIVE' => 'N',
					'NAME' => 'Владимир ОПТ',
					'1C_CODE' => '35',
					'PHONE' => array('79217232755'),
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '863',
					'HAS_EXPORT_Q' => true, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'EXPORT_MOVE_TO' => 36,
					'COORD' => array('56.091152', '40.238930'), // координаты склада (для оптовых менеджеров)
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
				),
			),
			'EXTERNAL_STORES' => array(
				'ivanovo' => array(
					'STORES' => array(
						1336,
					),
				),
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '17', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array("Вичуга", "Родники"),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '314778',
			'CALL_CENTER_PHONE_NUM' => '+7 (4922) 22-22-00',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4922) 22-22-00',
			'LIGHT_STORE' => true
		),
		/*s-city*/
		'vologda' => array(
			'NEW_SCHEME_ID' => '4',
			'ACTIVE' => 'Y',
			'NAME' => 'Вологда',
			'NAME2' => 'Вологде',
			'NAME3' => 'Володге',
			'ISO_REGION' => 'RU-VLG',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '16',
			'KLADR' => '35000001000',
			'REGION_KLADR' => '35',
			'PRICE_ID' => 2,
			'PRICE_CODE' => 'VOLOGDA',
			'QUANTITY_CODE' => 'Q_VOLOGDA',
			'OPT_STORE_ID' => 16, //23, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik', 'tech', 'akc',
				'wh', 'esp', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14', 'orsi', 'oslt',
				'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("95.53.128.34"),
			'DEFAULT_B2C' => 16, // для фидов
			'MAPS_COORDS' => array("59.22556173748073", "39.8806625"), // координаты города для карт
			'STORES' => array(
				'16' => array(
					'NEW_SCHEME_ID' => '16',
					'CITY_CODE' => 'vologda',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Вологда ТЦ',
					'MAIN' => true,
					'1C_CODE' => '16',
					'ADRESS_SKLADA' => 'г. Вологда, Окружное шоссе, д.18',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'Окружное шоссе, д.18',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('79301227042'), // Суворов
					'PHONE2' => array('79212430001'), // Ермолинский
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '129',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 23, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 7,
					'VACANCY_FORM_CODE' => 7,
					'CRM_FORM_CODE' => 7,
				),
				'23' => array(
					'CITY_CODE' => 'vologda',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Вологда Пр',
					'1C_CODE' => '23',
					'PHONE' => array('79217232755'),
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '130',
					'HAS_EXPORT_Q' => true, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'EXPORT_MOVE_TO' => 16,
					'COORD' => array('59.222746', '39.844492'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,//TODO: Опечатка же в названии параметра
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '230197',
			'CALL_CENTER_PHONE_NUM' => '+7 (8172) 28-28-28',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (8172) 28-28-28',
			'LIGHT_STORE' => false,
			"MANIPULATOR" => true,
		),
		/*s-city*/
		'ivanovo' => array(
			'NEW_SCHEME_ID' => '5',
			'ACTIVE' => 'Y',
			'NAME' => 'Иваново',
			'NAME2' => 'Иванове',
			'NAME3' => 'Иваново',
			'ISO_REGION' => 'RU-IVA',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '13',
			'KLADR' => '37000001000',
			'REGION_KLADR' => '37',
			'DIFF_PRICE' => true,
			'PRICE_ID' => 3,
			'PRICE_CODE' => 'IVANOVO',
			'QUANTITY_CODE' => 'Q_IVANOVO',
			'OPT_STORE_ID' => 13, // 28, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("80.70.97.242", "87.255.230.51"),
			'DEFAULT_B2C' => 13, // для фидов
			'MAPS_COORDS' => array("57.00858312487194", "40.996693499999985"), // координаты города для карт
			'STORES' => array(
				'13' => array(
					'NEW_SCHEME_ID' => '13',
					'CITY_CODE' => 'ivanovo',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Иваново',
					'MAIN' => true,
					'1C_CODE' => '13',
					'ADRESS_SKLADA' => 'г. Иваново, пр-кт Текстильщиков, д.80',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'пр-кт Текстильщиков, д.80',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('79203754787'), // Смирнов А.В.
					'PHONE2' => array('79631506449'), // Рачковский И.
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '131',
					"DIFF_PRICE_ID" => 3, // разделенная цена
					"DIFF_PRICE_CODE" => 'IVANOVO', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 28, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 6,
					'VACANCY_FORM_CODE' => 6,
					'CRM_FORM_CODE' => 6,
				),
				'28' => array(
					'CITY_CODE' => 'ivanovo',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Иваново Главснаб',
					'1C_CODE' => '28',
					'PHONE' => array('79065120601'),
					'PHONE2' => array('79022424414', '79807379737'), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '456',
					"DIFF_PRICE_ID" => 3, // разделенная цена
					"DIFF_PRICE_CODE" => 'IVANOVO', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('56.957419', '40.980589'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
				),
				'1336' => array(
					'NEW_SCHEME_ID' => '1336',
					'CITY_CODE' => 'ivanovo',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Иваново - Владимир',
					'1C_CODE' => '1336',
					'PHONE' => array('79203754787'),
					'PHONE2' => array('79631506449'), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '917',
					"DIFF_PRICE_ID" => 3, // разделенная цена
					"DIFF_PRICE_CODE" => 'IVANOVO', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => false, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
					'MAP' => true,
					'IS_EXT_AS_LOCAL' => false,

				),

			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '230164',
			'CALL_CENTER_PHONE_NUM' => '+7 (4932) 26-00-00',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4932) 26-00-00',
			'LIGHT_STORE' => false
		),
		/*s-city*/
		'kineshma' => array(
			'NEW_SCHEME_ID' => '6',
			'ACTIVE' => 'Y',
			'NAME' => 'Кинешма',
			'NAME2' => 'Кинешме',
			'NAME3' => 'Кинешме',
			'ISO_REGION' => 'RU-IVA',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => true,
			'1C_CODE' => '33',
			'KLADR' => '37007001000',
			'REGION_KLADR' => '37',
			'PRICE_ID' => 12,
			'PRICE_CODE' => 'KINESHMA',
			'QUANTITY_CODE' => 'Q_KINESHMA',
			'SHOW_IN_MENU' => 'Y',
			'EXTERNAL_SUPPLIERS' => array(  /* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'OPT_STORE_ID' => 33, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array("91.219.48.81"),
			'DEFAULT_B2C' => 33, // для фидов
			'MAPS_COORDS' => array("57.44219346344536", "42.15780600000002"), // координаты города для карт
			'STORES' => array(
				'33' => array(
					'NEW_SCHEME_ID' => '33',
					'CITY_CODE' => 'kineshma',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Кинешма',
					'MAIN' => true,
					'1C_CODE' => '33',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'Y',
					'ADRESS_SKLADA' => 'г. Кинешма, ул. Максима Горького, 22',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Максима Горького, 22',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '606',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 18:00',
					'REZERV_DO' => '18:00', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => true, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-18:00"),
					'APPEAL_FORM_CODE' => 10,
					'VACANCY_FORM_CODE' => 10,
					'CRM_FORM_CODE' => 10,
				),
			),
			'EXTERNAL_STORES' => array(
				'ivanovo' => array(
					'STORES' => array(
						13
					),
				),
				/*'kostroma' => array(
					'STORES' => array(
						2
					),
				),*/
			),
			'EXTERNAL_CITY_DAYS' => '2 дня',
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array("Вичуга", "Родники"),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '292810',
			'CALL_CENTER_PHONE_NUM' => '+7 (4932) 26-00-00',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4932) 26-00-00',
			'LIGHT_STORE' => true,
			'ORDER_CREATE_PAYMENT_TIME_TO_CHOOSE_DATE' => '17:00', //время, до которого при создании заказа требуем оплатить его сегодня, иначе завтра
			'ORDER_CREATE_PAYMENT_REQUIRE_TIME' => '18:00',    //время, до которого нужно оплатить заказ для дальнейшей его обработки
		),
		/*s-city*/
		'kirov' => array(
			'ACTIVE' => 'N',
			'NAME' => 'Киров',
			'NAME2' => 'Кирове',
			'NAME3' => 'Кирову',
			'ISO_REGION' => 'RU-KIR',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '34',
			'KLADR' => '43000001000',
			'REGION_KLADR' => '43',
			'PRICE_ID' => 6,
			'PRICE_CODE' => 'KIROV',
			'QUANTITY_CODE' => 'Q_KIROV',
			'SHOW_IN_MENU' => 'N',
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' , '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar' ,
					'mir' , 'orange' , 'ormatek' , 'lesenka_m', 'siver' , 'masterpol'  , 'stin', 'grohe' , 'sbk' , 'kurazh' , 'vemf', 'triya'
					, 'sigma_trade' , 'interflame' , 'bremen' , 'avens' , 'smm' , 'skl' , 'italmac' , 'vtl' , 'sto' , 'slavd' , 'nk',
					'venart' , 'mba' , 'mbio' , 'tehc' , 'mrgr' , 'otmk'  ,'ecod' , 'artm' , 'elm' , 'mmax' , 'sant' , 'prof' , 'dmtk' , 'bik' ,
					'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck' , 'opus' , 'lider' ,'monarh' , 'monarh7','rusroyal' , 'bk_centr7' , 'bk_centr14' ,
					 'orsi', 'oslt' , */),
			'OPT_STORE_ID' => 34, //25, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array("212.46.208.30", "92.255.231.128"),
			'DEFAULT_B2C' => 34, // для фидов
			'MAPS_COORDS' => array("58.58267654585965", "49.570856499999984"), // координаты города для карт
			'STORES' => array(
				'34' => array(
					'CITY_CODE' => 'kirov',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Киров',
					'MAIN' => true,
					'1C_CODE' => '34',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Киров, ул.Московская, д.114',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул.Московская, д.114',
					'SHOW_PICKUP' => true,
					'PHONE' => array('79229778282'),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '187',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('58.601893', '49.625586'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:30', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
				),
				'25' => array(
					'CITY_CODE' => 'kirov',
					'VISIBILITY' => 0, // отображение склада на сайте
					'ACTIVE' => "N",
					'NAME' => 'Киров Главснаб',
					'1C_CODE' => '25',
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '987',
					"DIFF_PRICE_ID" => 15, // разделенная цена
					"DIFF_PRICE_CODE" => 'KIROV_GLAVSNAB', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('58.601893', '49.625586'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,

				),
			),
			'EXTERNAL_STORES' => array(/*
				'kostroma' => array(
					'STORES' => array(
						31,
					),
				),

				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
				*/
			),
			'EXTERNAL_CITY_DAYS' => '2 дня',
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '4900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '220913',
			'CALL_CENTER_PHONE_NUM' => '+7 (8332) 22-00-22',
			'LIGHT_STORE' => false
		),
		/*s-city*/
		'kostroma' => array(
			'NEW_SCHEME_ID' => '1',
			'ACTIVE' => 'Y',
			'NAME' => 'Кострома',
			'NAME2' => 'Костроме',
			'NAME3' => 'Костроме',
			'ISO_REGION' => 'RU-KOS',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '2',
			'KLADR' => '44001001000',
			'REGION_KLADR' => '44',
			'PRICE_ID' => 4,
			'DIFF_PRICE' => true, // цены в этом городе разделены
			'PRICE_CODE' => 'KOSTROMA',
			'QUANTITY_CODE' => 'Q_KOSTROMA',
			'CREDIT_IS_AVAILABLE' => 'Y',
			'OPT_STORE_ID' => 2, // 12, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', '246', 'panno', 'aquanet', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik', 'akc',
				'tech', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("46.42.5.218"),
			'DEFAULT_B2C' => 2, // для фидов
			'MAPS_COORDS' => array("57.77302079714255", "40.90140849999999"), // координаты города для карт
			'STORES' => array(
				'2' => array(
					'NEW_SCHEME_ID' => '2',
					'CITY_CODE' => 'kostroma',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Кострома ТЦ',
					'MAIN' => true,
					'1C_CODE' => '2',
					'ADRESS_SKLADA' => 'г. Кострома, ул.Сутырина, д.3',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул.Сутырина, д.3',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('79159268414'), // Варенцов А.М.
					'PHONE2' => array('79159276310'), // Зонов И.
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '132',
					"DIFF_PRICE_ID" => 4, // разделенная цена
					"DIFF_PRICE_CODE" => 'KOSTROMA', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 12, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 3,
					'VACANCY_FORM_CODE' => 3,
					'CRM_FORM_CODE' => 3,
				),
				'12' => array(
					'CITY_CODE' => 'kostroma',
					'VISIBILITY' => 0, // отображение склада на сайте
					'ACTIVE' => 'N',
					'NAME' => 'Кострома Главснаб',
					'1C_CODE' => '12',
					'PHONE' => array('79536670797', '79108085825'),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '133',
					"DIFF_PRICE_ID" => 10, // разделенная цена
					"DIFF_PRICE_CODE" => 'KOSTROMA_GLAVSNAB', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('57.776596', '41.005802'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
				),
				'31' => array(
					'CITY_CODE' => 'kostroma',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Кострома РЦ',
					'1C_CODE' => '31',
					'PHONE' => array('79536670797', '79108085825'),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '496',
					"DIFF_PRICE_ID" => 11, // разделенная цена
					"DIFF_PRICE_CODE" => 'KOSTROMA_GLAVSNAB', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => true, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
				),
				'999' => array(
					'NEW_SCHEME_ID' => '999',
					'CITY_CODE' => 'kostroma',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Кострома Стройдвор',
					'1C_CODE' => '999',
					'PHONE' => array('79536670797', '79108085825'),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '1525',
					"DIFF_PRICE_ID" => 4, // разделенная цена
					"DIFF_PRICE_CODE" => 'KOSTROMA', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => false, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
					'MAP' => true,
					'IS_EXT_AS_LOCAL' => false,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 18.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '216747',
			'CALL_CENTER_PHONE_NUM' => '+7 (4942) 222-000',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4942) 222-000',
			'LIGHT_STORE' => false
		),
		'chehov' => array(
			'NEW_SCHEME_ID' => '15',
			'ACTIVE' => 'N',
			'NAME' => 'Чехов',
			'NAME2' => 'Чехове',
			'NAME3' => 'Чехову',
			'ISO_REGION' => 'RU-MOS',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '53',
			'KLADR' => '5003700100000',
			'REGION_KLADR' => '50',
			'PRICE_ID' => 26,
			'DIFF_PRICE' => false, // цены в этом городе разделены
			'PRICE_CODE' => 'CHEHOV_RC',
			'QUANTITY_CODE' => 'Q_CHEHOV',
			'CREDIT_IS_AVAILABLE' => 'Y',
			'OPT_STORE_ID' => 53, // 12, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', '246', 'panno', 'aquanet', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik', 'akc',
				'tech', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array(""),
			'DEFAULT_B2C' => 53, // для фидов
			'MAPS_COORDS' => array("57.77302079714255", "40.90140849999999"), // координаты города для карт
			'STORES' => array(
				'53' => array(
					'NEW_SCHEME_ID' => '53',
					'CITY_CODE' => 'chehov',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Чехов РЦ',
					'1C_CODE' => '53',
					'PHONE' => array(''),
					'PHONE2' => array(''), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '2437',
					"DIFF_PRICE_ID" => 26, // разделенная цена
					"DIFF_PRICE_CODE" => 'CHEHOV_RC', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => true, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
				)
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/

				/*'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),*/
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 18.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '4900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '',
			'YANDEX_COUNTER_ID' => '',
			'CALL_CENTER_PHONE_NUM' => '+7 (4942) 222-000',
			'LIGHT_STORE' => false
		),
		/*s-city*/
		'moskva' => array(
			'ACTIVE' => 'N',
			'NAME' => 'Мытищи',
			'NAME2' => 'Мытищах',
			'NAME3' => 'Мытищам',
			'ISO_REGION' => 'RU-MOS',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '14',
			'KLADR' => '7700000000000',
			'REGION_KLADR' => '77',
			'PRICE_ID' => 14,
			'PRICE_CODE' => 'MOSKVA',
			'QUANTITY_CODE' => 'Q_MOSKVA',
			'SHOW_IN_MENU' => 'Y',
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' , */
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains'), // Здесь перечисляем какие внешние поставщики работают в данном городе;
			'OPT_STORE_ID' => 14, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array( /*"89.109.236.75", "85.21.93.110" */),
			'DEFAULT_B2C' => 14, // для фидов
			'MAPS_COORDS' => array("55.72504493415047", "37.64696099999997"), // координаты города для карт
			'STORES' => array(
				'14' => array(
					'CITY_CODE' => 'moskva',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Мытищи',
					'MAIN' => true,
					'1C_CODE' => '14',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Мытищи, Ярославское шоссе, д.65',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'Ярославское шоссе, д.65',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Максимальная сумма гаказа при которой грузчики доступны
					'PROP_ID' => '969', // ID свойства "Остаток" для данного города
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('55.909092', '37.768694'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'пн-сб с 8:00 до 21:00, вс - выходной',
					'REZERV_DO' => '20:30', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => true, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00", "08:00-21:00", "08:00-21:00", "08:00-21:00", "08:00-21:00", "08:00-21:00"),
					'APPEAL_FORM_CODE' => 1,
					'VACANCY_FORM_CODE' => 1,
					'CRM_FORM_CODE' => 1,
				),
			),
			'EXTERNAL_STORES' => array(
				'yaroslavl' => array(
					'STORES' => array(
						4
					),
				),
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => false, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '15000',
			'FREE_DELIVERY_PRICE_M' => '5900',
			'FREE_DELIVERY_PRICE_B2B' => '20000',
			'LOAD_SUM_SKLAD' => '0.2', // Стоимость подъема одного килограма на этаж - сейчас не используется
			'DELIVERY_X_HOUR' => '20', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '333789',
			'CALL_CENTER_PHONE_NUM' => '+7 (495) 122-20-20',
			'LIGHT_STORE' => true
		),
		'moskva54' => array(
			'NEW_SCHEME_ID' => '7',
			'ACTIVE' => 'Y',
			'NAME' => 'Москва',
			'NAME2' => 'Москве',
			'NAME3' => 'Москве',
			'ISO_REGION' => 'RU-MOS',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '50',
			'KLADR' => '7700000000000',
			'REGION_KLADR' => '77',
			'PRICE_ID' => 27,
			'PRICE_CODE' => 'MOSKVA54',
			'QUANTITY_CODE' => 'Q_MOSKVA54',
			'SHOW_IN_MENU' => 'Y',
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' , */
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'), // Здесь перечисляем какие внешние поставщики работают в данном городе;
			'OPT_STORE_ID' => 50, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array( /*"89.109.236.75", "85.21.93.110" */),
			'DEFAULT_B2C' => 50, // для фидов
			'MAPS_COORDS' => array("55.722801", "37.674984"), // координаты города для карт
			'STORES' => array(
				'50' => array(
					'NEW_SCHEME_ID' => '50',
					'CITY_CODE' => 'moskva54',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Москва',
					'MAIN' => false,
					'1C_CODE' => '50',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Москва, ул. 1-я Дубровская, д. 13а, строение 1',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. 1-я Дубровская, д. 13а, строение 1',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Максимальная сумма гаказа при которой грузчики доступны
					'PROP_ID' => '2460', // ID свойства "Остаток" для данного города
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('55.722801, 37.674984'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'пн-вс с 10:00 до 22:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("10:00-22:00"),
					'APPEAL_FORM_CODE' => 1,
					'VACANCY_FORM_CODE' => 1,
					'CRM_FORM_CODE' => 1,
				),
				'54' => array(
					'CITY_CODE' => 'moskva54',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Москва',
					'MAIN' => false,
					'1C_CODE' => '54',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Москва, МКАД 8й км, д3, корпус 2',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'МКАД 8й км, д3, корпус 2',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Максимальная сумма гаказа при которой грузчики доступны
					'PROP_ID' => '2460', // ID свойства "Остаток" для данного города
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('55.702860, 37.834066'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'пн-вс с 10:00 до 22:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("10:00-22:00"),
					'APPEAL_FORM_CODE' => 1,
					'VACANCY_FORM_CODE' => 1,
					'CRM_FORM_CODE' => 1,
				),
			),
			'EXTERNAL_STORES' => array(
				'chehov' => array(
					'STORES' => array(
						53
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						4,//454,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '9900',
			'FREE_DELIVERY_PRICE_M' => '5900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2', // Стоимость подъема одного килограма на этаж - сейчас не используется
			'DELIVERY_X_HOUR' => '20', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '333789',
			'CALL_CENTER_PHONE_NUM' => '+7 (495) 122-20-20',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (495) 122-20-20',
			'LIGHT_STORE' => false,
			'ORDER_CREATE_PAYMENT_TIME_TO_CHOOSE_DATE' => '21:00', //время, до которого при создании заказа требуем оплатить его сегодня, иначе завтра
			'ORDER_CREATE_PAYMENT_REQUIRE_TIME' => '21:30',    //время, до которого нужно оплатить заказ для дальнейшей его обработки
		),
		/*s-city*/
		'pereslavl' => array(
			'NEW_SCHEME_ID' => '18',
			'ACTIVE' => 'N',
			'NAME' => 'Переславль-Залесский',
			'NAME2' => 'Переславле',
			'NAME3' => 'Переславле',
			'ISO_REGION' => 'RU-YAR',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '37',
			'KLADR' => '37007001000',
			'REGION_KLADR' => '76',
			'PRICE_ID' => 17,
			'PRICE_CODE' => 'PERESLAVL',
			'QUANTITY_CODE' => 'Q_PERESLAVL',
			'SHOW_IN_MENU' => 'Y',
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'OPT_STORE_ID' => 37, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array("95.129.136.130"),
			'DEFAULT_B2C' => 37, // для фидов
			'MAPS_COORDS' => array("56.741297", "38.918656"), // координаты города для карт
			'STORES' => array(
				'37' => array(
					'NEW_SCHEME_ID' => '37',
					'CITY_CODE' => 'pereslavl',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Переславль',
					'MAIN' => true,
					'1C_CODE' => '37',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'Y',
					'ADRESS_SKLADA' => 'г. Переславль-Залесский, с. Большая Брембола ул. Центральная 54',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'с. Большая Брембола ул. Центральная 54',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '1201', // id свойства остаток;
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'REZHIM_RABOTY' => 'пн-сб 08:15 - 17:30, вс 08:15 - 16:00',
					'REZERV_DO' => '18:30', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => false,
					'IS_EXT_AS_LOCAL' => true, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:15-18:00"),
					'APPEAL_FORM_CODE' => 12,
					'VACANCY_FORM_CODE' => 12,
					'CRM_FORM_CODE' => 12,
				),
			),
			'EXTERNAL_STORES' => array(
				'yaroslavl' => array(
					'STORES' => array(
						437
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня',
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => false,
			"FIRST_FREE_DELIVERY" => false, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '356564',
			'CALL_CENTER_PHONE_NUM' => '',
//			'CALL_CENTER_PHONE_NUM' => '+7 (48535) 2-00-20',
			'CALL_CENTER_PHONE_NUM_B2B' => '',
//			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (48535) 2-00-20',
			'LIGHT_STORE' => false,
			'BLOCK_ORDER' => false,
			'PICKUP_ONLY' => true,
			'NO_SERVICES' => true,
			'NO_BONUSES' => true,
		),
		/*s-city*/
		'tambov' => array(
			'NEW_SCHEME_ID' => '8',
			'ACTIVE' => 'Y',
			'NAME' => 'Тамбов',
			'NAME2' => 'Тамбове',
			'NAME3' => 'Тамбову',
			'ISO_REGION' => 'RU-TMB',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '45',
			'KLADR' => '6800000400000',
			'REGION_KLADR' => '68',
			'PRICE_ID' => 23,
			'PRICE_CODE' => 'TAMBOV',
			'QUANTITY_CODE' => 'Q_TAMBOV',
			'OPT_STORE_ID' => 45,// 22, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array(/*"37.29.51.34", "213.221.57.218"*/),
			'DEFAULT_B2C' => 45, // для фидов
			'MAPS_COORDS' => array("52.721219", "41.452274"), // координаты города для карт
			'STORES' => array(
				'45' => array(
					'NEW_SCHEME_ID' => '45',
					'CITY_CODE' => 'tambov',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Тамбов ТЦ',
					'MAIN' => true,
					'1C_CODE' => '45',
					'ADRESS_SKLADA' => 'г. Тамбов, ул. Советская, 194Б',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Советская, 194Б',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array(/*'79212523142'*/),
					'PHONE2' => array(/*'79216860343', '79210570016'*/), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '1808',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 45, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 17,
					'VACANCY_FORM_CODE' => 17,
					'CRM_FORM_CODE' => 17,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '20', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '',
			'YANDEX_COUNTER_ID' => '442632',
			'CALL_CENTER_PHONE_NUM' => '+7 (4752) 42-70-70',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4752) 42-70-70',
			'LIGHT_STORE' => false
		),
		/*s-city*/
		'cherepovets' => array(
			'NEW_SCHEME_ID' => '9',
			'ACTIVE' => 'Y',
			'NAME' => 'Череповец Рыбинская',
			'NAME2' => 'Череповце',
			'NAME3' => 'Череповцу',
			'ISO_REGION' => 'RU-VLG',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '11',
			'KLADR' => '35000002000',
			'REGION_KLADR' => '35',
			'PRICE_ID' => 5,
			'PRICE_CODE' => 'CHEREPOVETS',
			'QUANTITY_CODE' => 'Q_CHEREPOVETS',
			'OPT_STORE_ID' => 11,// 22, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' , */
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("37.29.51.34", "213.221.57.218"),
			'DEFAULT_B2C' => 11, // для фидов
			'MAPS_COORDS' => array("59.12441790418", "37.8621339999999"), // координаты города для карт
			'STORES' => array(
				'11' => array(
					'NEW_SCHEME_ID' => '11',
					'CITY_CODE' => 'cherepovets',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Череповец Рыбинская ТЦ',
					'MAIN' => true,
					'1C_CODE' => '11',
					'ADRESS_SKLADA' => 'г. Череповец, ул. Рыбинская, д.59',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Рыбинская, д.59',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('79212523142'),
					'PHONE2' => array('79216860343', '79210570016'), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '134',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 22, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 4,
					'VACANCY_FORM_CODE' => 4,
					'CRM_FORM_CODE' => 4,
				),
				'22' => array(
					'CITY_CODE' => 'cherepovets',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Череповец ВЧ',
					'1C_CODE' => '22',
					'PHONE' => array('79212523142'), // Жаров М.Н.
					'PHONE2' => array('79215454542'), // Коновалов С.В.
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '135',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('59.113872', '37.972477'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '230199',
			'CALL_CENTER_PHONE_NUM' => '+7 (8202) 28-11-11',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (8202) 28-11-11',
			'LIGHT_STORE' => false
		),
		/*s-city*/
		'cherepovets_m' => array(
			'NEW_SCHEME_ID' => '19',
			'ACTIVE' => 'Y',
			'NAME' => 'Череповец Архангельская',
			'NAME2' => 'Череповце',
			'NAME3' => 'Череповцу',
			'ISO_REGION' => 'RU-VLG',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '47',
			'KLADR' => '35000002000',
			'REGION_KLADR' => '35',
			'PRICE_ID' => 30,
			'PRICE_CODE' => 'CHEREPOVETS_M',
			'QUANTITY_CODE' => 'Q_CHEREPOVETS_M',
			'OPT_STORE_ID' => 47, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' , */
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("37.29.51.34", "213.221.57.218"),
			'DEFAULT_B2C' => 47, // для фидов
			'MAPS_COORDS' => array("59.132111", "37.978255"), // координаты города для карт
			'STORES' => array(
				'47' => array(
					'NEW_SCHEME_ID' => '47',
					'CITY_CODE' => 'cherepovets_m',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Череповец Архангельская ТЦ',
					'MAIN' => true,
					'1C_CODE' => '47',
					'ADRESS_SKLADA' => 'г. Череповец, ул. Архангельская, д.43',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Архангельская, д.43',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('78202281111'),
					'PHONE2' => array('78202281111'), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '2800', // id свойства остаток;
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'REZHIM_RABOTY' => 'ежедневно с 10:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("10:00-21:00"),
					'APPEAL_FORM_CODE' => 22,
					'VACANCY_FORM_CODE' => 22,
					'CRM_FORM_CODE' => 22,
				),
			),
			'EXTERNAL_STORES' => array(
				'cherepovets' => array(
					'STORES' => array(
						11,
					),
				),
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на послезавтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '230199',
			'CALL_CENTER_PHONE_NUM' => '+7 (8202) 28-11-11',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (8202) 28-11-11',
			'LIGHT_STORE' => false,
			'PICKUP_ONLY' => true,
		),
		/*s-city*/
		'yaroslavl' => array(
			'NEW_SCHEME_ID' => '2',
			'ACTIVE' => 'Y',
			'NAME' => 'Ярославль',
			'NAME2' => 'Ярославле',
			'NAME3' => 'Ярославлю',
			'ISO_REGION' => 'RU-YAR',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '4',
			'KLADR' => '76000001000',
			'REGION_KLADR' => '76',
			'PRICE_ID' => 1,
			'DIFF_PRICE' => true, // цены в этом городе разделены
			'PRICE_CODE' => 'YAROSLAVL',
			'USE_PRICE_OF_STORE' => true,
			'QUANTITY_CODE' => 'Q_YAROSLAVL',
			'OPT_STORE_ID' => 4, //30, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(  /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("213.150.73.146", "77.220.57.182", "213.150.83.98", "46.42.5.218", "78.25.121.71"),
			'DEFAULT_B2C' => 4, // для фидов
			'MAPS_COORDS' => array("57.650721230284134", "39.86692249999997"), // координаты города для карт
			'STORES' => array(
				'4' => array(
					'NEW_SCHEME_ID' => '4',
					'CITY_CODE' => 'yaroslavl',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Ярославль Громова',
					'MAIN' => true,
					'DEFAULT' => true,
					'1C_CODE' => '4',
					'ADRESS_SKLADA' => 'г. Ярославль, ул.Громова, д.13',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул.Громова, д.13',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('79159983938'), // Капралов М.А.
					'PHONE2' => array('79159983938'), // Морозов Д.Ю.
					'LOAD_SUM_MAX' => '2000',
					'PAY_SYSTEM_ID_CARD' => 19,
					'PROP_ID' => '136',
					"DIFF_PRICE_ID" => 1, // разделенная цена
					"DIFF_PRICE_CODE" => 'YAROSLAVL', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 30, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 10:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("10:00-21:00"),
					'APPEAL_FORM_CODE' => 8,
					'VACANCY_FORM_CODE' => 8,
					'CRM_FORM_CODE' => 8,
				),
				'15' => array(
					'NEW_SCHEME_ID' => '15',
					'CITY_CODE' => 'yaroslavl',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Ярославль Фрунзе',
					'MAIN' => true,
					'1C_CODE' => '15',
					'ADRESS_SKLADA' => 'г. Ярославль, пр-кт Фрунзе, д.30',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'пр-кт Фрунзе, д.30',
					'SHOW_PICKUP' => true,
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'PHONE' => array('79206530155'), // Варакин А.В.
					'PHONE2' => array('79159742662'), // Лобанов Д.В.
					'LOAD_SUM_MAX' => '2000',
					'PAY_SYSTEM_ID_CARD' => 20,
					'PROP_ID' => '137',
					"DIFF_PRICE_ID" => 9, // разделенная цена
					"DIFF_PRICE_CODE" => 'YAROSLAVL_FR', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 30, // ID склада, с которого можно перемещать на данный
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 2,
					'VACANCY_FORM_CODE' => 2,
					'CRM_FORM_CODE' => 2,
				),
				'30' => array(
					'CITY_CODE' => 'yaroslavl',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Ярославль Гр. ОПТ',
					'1C_CODE' => '30',
					'PHONE' => array('79301052774'),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '457',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					//'MOVING_SRC_STORE' => 22, // ID склада, с которого можно перемещать на данный
					'COORD' => array('57.678179', '39.759294'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
				),
				'42' => array(
					'NEW_SCHEME_ID' => '42',
					'CITY_CODE' => 'yaroslavl',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Громова РЦ',
					'1C_CODE' => '42',
					'PHONE' => array('79206530155'), // Варакин А.В. ???
					'PHONE2' => array('79159742662'), // Лобанов Д.В. ???
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '1727',
					"DIFF_PRICE_ID" => 20, // разделенная цена
					"DIFF_PRICE_CODE" => 'YAROSLAVL_RC', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => true, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
					'MAP' => true,
					'IS_EXT_AS_LOCAL' => false,
				),
				'454' => array(
					'NEW_SCHEME_ID' => '454',
					'CITY_CODE' => 'yaroslavl',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Ярославль - Москва Драйв',
					'1C_CODE' => '454',
					'PHONE' => array('79159983938'),
					'PHONE2' => array('79159983938'),
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '2473',
					"DIFF_PRICE_ID" => 1, // разделенная цена
					"DIFF_PRICE_CODE" => 'YAROSLAVL', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => false, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
					'MAP' => true,
					'IS_EXT_AS_LOCAL' => false,
				),
				'437' => array(
					'NEW_SCHEME_ID' => '437',
					'CITY_CODE' => 'yaroslavl',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Ярославль - Переславль',
					'1C_CODE' => '437',
					'PHONE' => array('79159983938'),
					'PHONE2' => array('79159983938'),
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '2473',
					"DIFF_PRICE_ID" => 1, // разделенная цена
					"DIFF_PRICE_CODE" => 'YAROSLAVL', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => false, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
					'MAP' => true,
					'IS_EXT_AS_LOCAL' => false,
				),


			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => true,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 18.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '220367',
			'CALL_CENTER_PHONE_NUM' => '+7 (4852) 73-55-55',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4852) 73-55-55',
			'LIGHT_STORE' => false
			//'CALL_CENTER_PHONE_NUM' => '8-800-100-4900'
		),
		/*s-city*/
		'tutaev' => array(
			'NEW_SCHEME_ID' => '20',
			'ACTIVE' => 'Y',
			'NAME' => 'Тутаев',
			'NAME2' => 'Тутаеве',
			'NAME3' => 'Тутаеву',
			'ISO_REGION' => 'RU-YAR',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '504',
			'KLADR' => '76000001000',
			'REGION_KLADR' => '76',
			'PRICE_ID' => 31,
			'DIFF_PRICE' => true, // цены в этом городе разделены
			'PRICE_CODE' => 'TUTAEV',
			'USE_PRICE_OF_STORE' => false,
			'QUANTITY_CODE' => 'Q_TUTAEV',
			'OPT_STORE_ID' => 4, //30, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(  /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'italmac', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("213.150.73.146", "77.220.57.182", "213.150.83.98", "46.42.5.218", "78.25.121.71"),
			'DEFAULT_B2C' => 4, // для фидов
			'MAPS_COORDS' => array("57.868795", "39.530714"), // координаты города для карт
			'STORES' => array(
				'504' => array(
					'NEW_SCHEME_ID' => '504',
					'CITY_CODE' => 'tutaev',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Тутаев',
					'MAIN' => false,
					'1C_CODE' => '504',
					'REAL_1C_STORE' => '4', // будет участвовать в новой схеме для подмены значения склада в обмене
					'TERMINAL_SHOW_PICKUP' => 'N',
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => '',
					'ADRESS_SKLADA_WITHOUT_CITY' => '',
					'SHOW_PICKUP' => false,
					'PHONE' => array(),
					'PHONE2' => array(),
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '2815',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'REZHIM_RABOTY' => 'ежедневно с 10:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("10:00-21:00"),
					'APPEAL_FORM_CODE' => 8,
					'VACANCY_FORM_CODE' => 8,
					'CRM_FORM_CODE' => 8,
				),
			),
			'EXTERNAL_STORES' => array(
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'FAST_ORDER_SHOW_STORES' => true,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '220367',
			'CALL_CENTER_PHONE_NUM' => '+7 (4852) 73-55-55',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4852) 73-55-55',
			'LIGHT_STORE' => false,
			'DELIVERY_ONLY' => true,
		),
		/*s-city*/
		'ulyanovsk' => array(
			'ACTIVE' => 'N',
			'NAME' => 'Ульяновск',
			'NAME2' => 'Ульяновске',
			'NAME3' => 'Ульяновску',
			'ISO_REGION' => 'RU-ULY',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '27',
			'KLADR' => '73000001000',
			'REGION_KLADR' => '73',
			'PRICE_ID' => 7,
			'PRICE_CODE' => 'ULYANOVSK',
			'QUANTITY_CODE' => 'Q_ULYANOVSK',
			'EXTERNAL_SUPPLIERS' => array('yst', 'panno'),
			'OPT_STORE_ID' => 27, // ID склада, на который уходят оптовые заказы
			'MAPS_COORDS' => array("54.302987147665995", "48.43415949999991"), // координаты города для карт
			'STORES' => array(
				'27' => array(
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Ульяновск',
					'MAIN' => true,
					'1C_CODE' => '27',
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'SPEC_TOWN_TERMINAL_PICKUP' => 'Y',
					'ADRESS_SKLADA' => 'Московское ш. д.17а',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'Московское ш. д.17а',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '194',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('54.303649', '48.306159'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:30', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true
				),
			),
			/*'EXTERNAL_STORES' => array(
				'kostroma' => array(
					'STORES' => array(
						2, 31,
					),
				),
			),*/
			//'SHOW_IN_MENU' => 'N',// Не показывать в меню выбора города. Если не установлено или не равно N - показывается.
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '15000',
			'FREE_DELIVERY_PRICE_M' => '4900',
			'FREE_DELIVERY_PRICE_B2B' => '20000',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 5, 'delivery' => 5)),
				2 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				3 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 5, 'delivery' => 5)),
				4 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				5 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 6, 'delivery' => 6)),
				6 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 4, 'delivery' => 4), 2 => array('sam' => 5, 'delivery' => 5), 3 => array('sam' => 5, 'delivery' => 5)),
				7 => array(0 => array('sam' => 3, 'delivery' => 3), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '230161',
			'CALL_CENTER_PHONE_NUM' => '+7 (8422) 306-000',
			'LIGHT_STORE' => false
		),
		/*s-city*/
		'rybinsk' => array(
			'NEW_SCHEME_ID' => '10',
			'ACTIVE' => 'Y',
			'NAME' => 'Рыбинск',
			'NAME2' => 'Рыбинске',
			'NAME3' => 'Рыбинску',
			'ISO_REGION' => 'RU-YAR',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '29',
			'KLADR' => '7601500100000',
			'REGION_KLADR' => '76',
			'PRICE_ID' => 8,
			'PRICE_CODE' => 'RYBINSK',
			'QUANTITY_CODE' => 'Q_RYBINSK',
			'EXTERNAL_SUPPLIERS' => array(  /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'OPT_STORE_ID' => 29, // ID склада, на который уходят оптовые заказы
			'TERMINALS' => array("178.218.43.22", "80.92.31.63"),
			'DEFAULT_B2C' => 29, // для фидов
			'MAPS_COORDS' => array("58.06143074830594", "38.812502499999965"), // координаты города для карт
			'STORES' => array(
				'29' => array(
					'NEW_SCHEME_ID' => '29',
					'CITY_CODE' => 'rybinsk',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Рыбинск',
					'MAIN' => true,
					'1C_CODE' => '29',
					'TERMINAL_SHOW_PICKUP' => 'Y',
					'ADRESS_SKLADA' => 'г. Рыбинск, ул.Суркова, д.2',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул.Суркова, д.2',
					'SHOW_PICKUP' => true,
					'PHONE' => array('79201276447'), // Рыжов А.В.
					'PHONE2' => array('79807401929'), // Воронов С.А.
					'LOAD_SUM_MAX' => '2000',
					'PROP_ID' => '271',
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'MOVING_SRC_STORE' => 30, // ID склада, с которого можно перемещать на данный
					'COORD' => array('58.049983', '38.769662'), // координаты склада (для оптовых менеджеров)
					'DELIVERY_PERSENT_X' => 2,
					'DELIVERY_PERSENT_Y' => 4,
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00',
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 5,
					'VACANCY_FORM_CODE' => 5,
					'CRM_FORM_CODE' => 5,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31,
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
				/*
				'yaroslavl' => array(
					'STORES' => array(
						4
					),
				),*/
			),
			//'SHOW_IN_MENU' => 'Y',// Не показывать в меню выбора города. Если не установлено или не равно N - показывается.
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			//'FREE_DELIVRY_PRICE' => '15000', //по акции с 24.10 - 10000
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			//'FREE_DELIVERY_PRICE_B2B' => '20000', //по акции с 24.10 - 15000
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '16', // Час до которого принимается заказы с доставкой на завтра, после уже на после завтра
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array("Ермаково"),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '240539',
			'CALL_CENTER_PHONE_NUM' => '+7 (4852) 73-55-55',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4852) 73-55-55',
			'LIGHT_STORE' => false
		),

		/*s-city*/
		'dzerzhinsk' => array(
			'NEW_SCHEME_ID' => '11',
			'ACTIVE' => 'Y',
			'NAME' => 'Дзержинск',
			'NAME2' => 'Дзержинске',
			'NAME3' => 'Дзержинску',
			'ISO_REGION' => 'RU-NIZ',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '41',
			'KLADR' => '3300000100000',
			'REGION_KLADR' => '52',
			'PRICE_ID' => 19,
			'PRICE_CODE' => 'DZERZHINSK',
			'QUANTITY_CODE' => 'Q_DZERZHINSK',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 41, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'realk', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("95.172.38.94", "89.109.22.50"), // ДЗЕРЖИНСК ПРАВИТЬ
			'DEFAULT_B2C' => 41, // для фидов
			'MAPS_COORDS' => array("56.242188735787444", "43.50030699999996"), // координаты города для карт
			'STORES' => array(
				'41' => array(
					'NEW_SCHEME_ID' => '41',
					'CITY_CODE' => 'dzerzhinsk',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Дзержинск',
					'MAIN' => true,
					'1C_CODE' => '41',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Дзержинск, ул. Красноармейская, 15E',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Красноармейская, 15E',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1517', // ID свойства "Остаток" для данного города '1159'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('56.247470', '43.404966'), // координаты склада (для оптовых менеджеров)
					'REZHIM_RABOTY' => 'ежедневно с 9:00 до 20:00', //режим работы
					'REZERV_DO' => '19:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("09:00-20:00"),
					'APPEAL_FORM_CODE' => 14,
					'VACANCY_FORM_CODE' => 14,
					'CRM_FORM_CODE' => 14,
				),
			),
			'EXTERNAL_STORES' => array(
				'nnovgorod' => array(
					'STORES' => array(
						436
					),
				),
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				/*'kostroma' => array(
					'STORES' => array(
						31
					),
				),*/
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array("Вичуга", "Родники"),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '394715',
			'CALL_CENTER_PHONE_NUM' => '+7 (8313) 239-888',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (8313) 239-888',
			'LIGHT_STORE' => false,
			'ORDER_CREATE_PAYMENT_TIME_TO_CHOOSE_DATE' => '19:00', //время, до которого при создании заказа требуем оплатить его сегодня, иначе завтра
			'ORDER_CREATE_PAYMENT_REQUIRE_TIME' => '12:00',    //время, до которого нужно оплатить заказ для дальнейшей его обработки
		),
		/*s-city*/
		'kaluga' => array(
			'NEW_SCHEME_ID' => '12',
			'ACTIVE' => 'Y',
			'NAME' => 'Калуга Московская',
			'NAME2' => 'Калуге',
			'NAME3' => 'Калуге',
			'ISO_REGION' => 'RU-KAL',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '44',
			'KLADR' => '4000000100000', // ???
			'REGION_KLADR' => '40',    // ???
			'PRICE_ID' => 21,
			'PRICE_CODE' => 'KALUGA',
			'QUANTITY_CODE' => 'Q_KALUGA',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 44, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'amega', 'elmat'),
			'TERMINALS' => array("95.172.38.94", "89.109.22.50"),    // ПРАВИТЬ
			'DEFAULT_B2C' => 44, // для фидов
			'MAPS_COORDS' => array("54.51372563960182", "36.26281224121091"), // координаты города для карт
			'STORES' => array(
				'44' => array(
					'NEW_SCHEME_ID' => '44',
					'CITY_CODE' => 'kaluga',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Калуга Московская',
					'MAIN' => true,
					'1C_CODE' => '44',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Калуга, Московская улица, 338А',    // ПРАВИТЬ
					'ADRESS_SKLADA_WITHOUT_CITY' => 'Московская улица, 338А', // ПРАВИТЬ
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1769', // ID свойства "Остаток" для данного города '1769'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('54.586655, 36.247911'), // координаты склада (для оптовых менеджеров) ПРАВИТЬ
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00', //режим работы ПРАВИТЬ
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе ПРАВИТЬ
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"), // ПРАВИТЬ
					'APPEAL_FORM_CODE' => 16,
					'VACANCY_FORM_CODE' => 16,
					'CRM_FORM_CODE' => 16,
				),
			),
			'EXTERNAL_STORES' => array(
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '431751',
			'CALL_CENTER_PHONE_NUM' => '+7 (4842) 212-002',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4842) 212-002',
			'LIGHT_STORE' => false,
		),
		/*s-city*/
		/*крайне важно проверить и исправить все значения перед запуском. сейчас город используется только для заполнения сроков доставки от внешних поставщиков*/
		'kulaga' => array(
			'ACTIVE' => 'Y',
			'NAME' => 'Калуга Болдина',
			'NAME2' => 'Калуге',
			'NAME3' => 'Калуге',
			'ISO_REGION' => 'RU-KAL',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '46',
			'KLADR' => '4000000100000', // ???
			'REGION_KLADR' => '40',    // ???
			'PRICE_ID' => 25,
			'PRICE_CODE' => 'KULAGA',
			'QUANTITY_CODE' => 'Q_KULAGA',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 46, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' , */
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'amega', 'elmat'),
			'TERMINALS' => array(),    // ПРАВИТЬ
			'DEFAULT_B2C' => 46, // для фидов
			'MAPS_COORDS' => array("54.51372563960182", "36.26281224121091"), // координаты города для карт
			'STORES' => array(
				'46' => array(
					'NEW_SCHEME_ID' => '46',
					'CITY_CODE' => 'kulaga',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Калуга Болдина',
					'MAIN' => true,
					'1C_CODE' => '46',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Калуга, ул.Болдина, 87, к.1',
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул.Болдина, 87, к.1',
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1840', // ID свойства "Остаток" для данного города '1840'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('54.500696, 36.304343'), // координаты склада (для оптовых менеджеров)
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00', //режим работы
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"),
					'APPEAL_FORM_CODE' => 18,
					'VACANCY_FORM_CODE' => 18,
					'CRM_FORM_CODE' => 18,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '431751',
			'CALL_CENTER_PHONE_NUM' => '+7 (4842) 212-002',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4842) 212-002',
			'LIGHT_STORE' => false,
		),
		/*s-city*/
		/*крайне важно проверить и исправить все значения перед запуском. сейчас город используется только для заполнения сроков доставки от внешних поставщиков*/
		'smolensk' => array(
			'NEW_SCHEME_ID' => '13',
			'ACTIVE' => 'Y',
			'NAME' => 'Смоленск',
			'NAME2' => 'Смоленске',
			'NAME3' => 'Смоленску',
			'ISO_REGION' => 'RU-KAL',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '48',
			'KLADR' => '6700000000000', // ???
			'REGION_KLADR' => '67',    // ???
			'PRICE_ID' => 24,
			'PRICE_CODE' => 'SMOLENSK',
			'QUANTITY_CODE' => 'Q_SMOLENSK',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 48, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array(),    // ПРАВИТЬ
			'DEFAULT_B2C' => 48, // для фидов
			'MAPS_COORDS' => array("54.782640", "32.045134"), // координаты города для карт
			'STORES' => array(
				'48' => array(
					'NEW_SCHEME_ID' => '48',
					'CITY_CODE' => 'smolensk',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Смоленск',
					'MAIN' => true,
					'1C_CODE' => '48',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Смоленск, ул. Шевченко, 86',    // ПРАВИТЬ
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Шевченко, 86', // ПРАВИТЬ
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1834', // ID свойства "Остаток" для данного города '1769'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('54.779120, 32.084175'), // координаты склада (для оптовых менеджеров) ПРАВИТЬ
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00', //режим работы ПРАВИТЬ
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе ПРАВИТЬ
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"), // ПРАВИТЬ
					'APPEAL_FORM_CODE' => 19,
					'VACANCY_FORM_CODE' => 19,
					'CRM_FORM_CODE' => 19,
				),
			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '447289',
			'CALL_CENTER_PHONE_NUM' => '+7 (4812) 339-888',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4812) 339-888',
			'LIGHT_STORE' => false,
		),
		'nnovgorod' => array(
			'NEW_SCHEME_ID' => '14',
			'ACTIVE' => 'Y',
			'NAME' => 'Нижний Новгород',
			'NAME2' => 'Нижнем Новгороде',
			'NAME3' => 'Нижнему Новгороду',
			'ISO_REGION' => 'RU-NOV',
			'REGIONAL_CENTER' => true,
			'DEFAULT' => false,
			'1C_CODE' => '43',
			'KLADR' => '5200000100000', // ???
			'REGION_KLADR' => '52',    // ???
			'DIFF_PRICE' => true,
			'PRICE_ID' => 22,
			'PRICE_CODE' => 'NNOVGOROD',
			'QUANTITY_CODE' => 'Q_NNOVGOROD',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 43, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array(/* 'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array("95.172.38.94", "89.109.22.50", "95.80.75.61", "95.79.102.105"),    // ПРАВИТЬ
			'DEFAULT_B2C' => 43, // для фидов
			'MAPS_COORDS' => array("56.32282100213566", "43.99419854882801"), // координаты города для карт
			'STORES' => array(
				'43' => array(
					'NEW_SCHEME_ID' => '43',
					'CITY_CODE' => 'nnovgorod',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Нижний Новгород',
					'MAIN' => true,
					"DIFF_PRICE_ID" => 22, // разделенная цена
					"DIFF_PRICE_CODE" => 'NNOVGOROD', // код разделенной цены
					'1C_CODE' => '43',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Нижний Новгород, Комсомольское шоссе, д. 16',    // ПРАВИТЬ
					'ADRESS_SKLADA_WITHOUT_CITY' => 'Комсомольское шоссе, д. 16', // ПРАВИТЬ
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '1771', // ID свойства "Остаток" для данного города '1769'
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('56.310403, 43.897605'), // координаты склада (для оптовых менеджеров) ПРАВИТЬ
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 21:00', //режим работы ПРАВИТЬ
					'REZERV_DO' => '20:20', //до скольки резервируется товар при самовывозе ПРАВИТЬ
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-21:00"), // ПРАВИТЬ
					'APPEAL_FORM_CODE' => 15,
					'VACANCY_FORM_CODE' => 15,
					'CRM_FORM_CODE' => 15,
				),
				'436' => array(
					'NEW_SCHEME_ID' => '436',
					'CITY_CODE' => 'nnovgorod',
					'VISIBILITY' => 0, // отображение склада на сайте
					'NAME' => 'Н.Новгород Стройдвор',
					'1C_CODE' => '436',
					'PHONE' => array('79536670797', '79108085825'),
					'PHONE2' => array('79607393354'), // кладовщики
					'LOAD_SUM_MAX' => '2250',
					'PROP_ID' => '1787',
					"DIFF_PRICE_ID" => 22, // разделенная цена
					"DIFF_PRICE_CODE" => 'NNOVGOROD', // код разделенной цены
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'LOADER_AVAILABLE' => true,
					'DC' => false, // распределительный центр (distribution center) Не удаляйте ПЖСТ!
					'MAP' => true,
					'IS_EXT_AS_LOCAL' => false,
					'APPEAL_FORM_CODE' => 15,
					'VACANCY_FORM_CODE' => 15,
					'CRM_FORM_CODE' => 15,
				),

			),
			'EXTERNAL_STORES' => array(
				/*'kostroma' => array(
					'STORES' => array(
						31
					),
				),*/
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '430446',
			'CALL_CENTER_PHONE_NUM' => '+7 (831) 215-50-50',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (831) 215-50-50',
			'LIGHT_STORE' => false,
		),
		'kovrov' => array(
			'NEW_SCHEME_ID' => '16',
			'ACTIVE' => 'Y',
			'NAME' => 'Ковров',
			'NAME2' => 'Коврове',
			'NAME3' => 'Коврову',
			'ISO_REGION' => 'RU-KOV',
			'REGIONAL_CENTER' => false,
			'DEFAULT' => false,
			'1C_CODE' => '49',
			'KLADR' => '3300000400000',
			'REGION_KLADR' => '33',
			'PRICE_ID' => 29,
			'PRICE_CODE' => 'KOVROV',
			'QUANTITY_CODE' => 'Q_KOVROV',
			'SHOW_IN_MENU' => 'Y',
			'OPT_STORE_ID' => 49, // ID склада, на который уходят оптовые заказы
			'EXTERNAL_SUPPLIERS' => array( /*'mrl' ,*/
				'yst', '246', 'panno', 'ovk-trade', 'dik', 'vasko', 'glazov', 'olimar',
				'mir', 'orange', 'ormatek', 'lesenka_m', 'siver', 'masterpol', 'stin', 'grohe', 'sbk', 'kurazh', 'vemf', 'triya'
			, 'sigma_trade', 'interflame', 'bremen', 'avens', 'smm', 'skl', 'vtl', 'sto', 'slavd', 'nk',
				'venart', 'mba', 'mbio', 'tehc', 'mrgr', 'otmk', 'ecod', 'artm', 'elm', 'mmax', 'sant', 'prof', 'dmtk', 'bik',
				'tech', 'akc', 'wh', 'esp', 'anr', 'mdeck', 'opus', 'lider', 'monarh', 'monarh7', 'rusroyal', 'bk_centr7', 'bk_centr14',
				'orsi', 'oslt', 'luc', 'ask', 'krsh', 'msvt', 'fmax', 'itana_mirror', 'itana_glass', 'itana_ldsp', 'curtains', 'elmat'),
			'TERMINALS' => array(),    // ПРАВИТЬ
			'DEFAULT_B2C' => 49, // для фидов
			'MAPS_COORDS' => array("56.363628", "41.311220"), // координаты города для карт
			'STORES' => array(
				'49' => array(
					'NEW_SCHEME_ID' => '49',
					'CITY_CODE' => 'kovrov',
					'VISIBILITY' => 1, // отображение склада на сайте
					'NAME' => 'Ковров',
					'MAIN' => true,
					'1C_CODE' => '49',
					'TERMINAL_SHOW_PICKUP' => 'Y',// Показывать самовывоз в терминалах. Если не равно Y - не показывается.
					'SPEC_TOWN_TERMINAL_PICKUP' => 'N',
					'ADRESS_SKLADA' => 'г. Ковров ул. Ватутина 55/1',    // ПРАВИТЬ
					'ADRESS_SKLADA_WITHOUT_CITY' => 'ул. Ватутина 55/1', // ПРАВИТЬ
					'SHOW_PICKUP' => true,
					'PHONE' => array(),
					'PHONE2' => array(), // кладовщики
					'LOAD_SUM_MAX' => '2000',// Хрен знает что за фигня такая
					'PROP_ID' => '2695', // ID свойства "Остаток" для данного города
					'HAS_EXPORT_Q' => false, // true, если данный склад является внешним для какого-либо города, и внешние остатки передаются в отдельном поле COUNTLOG
					'COORD' => array('56.339258, 41.315290'), // координаты склада (для оптовых менеджеров) ПРАВИТЬ
					'REZHIM_RABOTY' => 'ежедневно с 8:00 до 19:00', //режим работы ПРАВИТЬ
					'REZERV_DO' => '19:00', //до скольки резервируется товар при самовывозе ПРАВИТЬ
					'LOADER_AVAILABLE' => true,
					'IS_EXT_AS_LOCAL' => false, // остатки с внешних складов показываются как локальные (актуально для пунктов самовывоза)
					'USE_ONE_RATIO' => true,
					'WORK_TIME' => array("08:00-19:00"), // ПРАВИТЬ
					'APPEAL_FORM_CODE' => 21,
					'VACANCY_FORM_CODE' => 21,
					'CRM_FORM_CODE' => 21,
				),
			),
			'EXTERNAL_STORES' => array(
				'ivanovo' => array(
					'STORES' => array(
						13,
					),
				),
				'chehov' => array(
					'STORES' => array(
						53,
					),
				),
				'yaroslavl' => array(
					'STORES' => array(
						42,
					),
				),
			),
			'EXTERNAL_CITY_DAYS' => '2 дня', // Архаизм
			'FAST_ORDER_SHOW_STORES' => false,
			'FREE_DELIVRY' => true,
			"FIRST_FREE_DELIVERY" => true, // флаг доступности первой бесплатной доставки
			'FREE_DELIVRY_PRICE' => '6900',
			'FREE_DELIVERY_PRICE_M' => '2900',
			'FREE_DELIVERY_PRICE_B2B' => '15000',
			'FREE_DELIVERY_PRICE_B2B_M' => '7900',
			'LOAD_SUM_SKLAD' => '0.2',
			'DELIVERY_X_HOUR' => '18',
			'DELIVERY_CONFIG' => array(
				1 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				2 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				3 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 4, 'delivery' => 4)),
				4 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
				5 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 2, 'delivery' => 2), 3 => array('sam' => 5, 'delivery' => 5)),
				6 => array(0 => array('sam' => 1, 'delivery' => 1), 1 => array('sam' => 3, 'delivery' => 3), 2 => array('sam' => 4, 'delivery' => 4), 3 => array('sam' => 4, 'delivery' => 4)),
				7 => array(0 => array('sam' => 2, 'delivery' => 2), 1 => array('sam' => 2, 'delivery' => 2), 2 => array('sam' => 3, 'delivery' => 3), 3 => array('sam' => 3, 'delivery' => 3)),
			),
			"ALTER_CITIES" => array(),
			'YANDEX_WEBMASTER_ID' => '19737949',
			'YANDEX_COUNTER_ID' => '314778',
			'CALL_CENTER_PHONE_NUM' => '+7 (4932) 26-00-00',
			'CALL_CENTER_PHONE_NUM_B2B' => '+7 (4932) 26-00-00',
			'LIGHT_STORE' => false,
			'BLOCK_ORDER' => false,
			'ORDER_CREATE_PAYMENT_TIME_TO_CHOOSE_DATE' => '18:00', //время, до которого при создании заказа требуем оплатить его сегодня, иначе завтра
			'ORDER_CREATE_PAYMENT_REQUIRE_TIME' => '19:00',    //время, до которого нужно оплатить заказ для дальнейшей его обработки
		),
	);

	public static function GetList($onlyActive = false)
	{

		$arCity2 = static::$arCity;

		if ($onlyActive) {
			foreach ($arCity2 as $cityCode => $cityData) {
				if ($cityData["ACTIVE"] !== "Y") unset($arCity2[$cityCode]);
			}
		}

		uasort($arCity2, "self::cmp");
		return $arCity2;
	}
/*
    public static function GetFranchCities (){

        $globalCities = array();
        $arFilter = array('IBLOCK_ID' => 52, 'ACTIVE' => 'Y');
        while($ar_result = $secRes->GetNext())
        {
            $globalCities[ $ar_result['XML_ID'] ] = $ar_result;
            $globalCities[ $ar_result['XML_ID'] ]['FRANCH'] = array();

            $arFilter = array(
                "IBLOCK_ID" => 52,
                "ACTIVE" => "Y",
                "SECTION_ID" => $ar_result['ID']
            );
            $arSelect = array(
                'ID',
                'IBLOCK_ID',
                'NAME',
                'PROPERTY_OWN',
                'PROPERTY_REAL_CITY',
                'PROPERTY_DELIVERY',
            );
            $res = CIBlockElement::GetList(array("NAME" => "ASC"), $arFilter, $arSelect);
            while($ar_fields = $res->GetNext()){
                $ar_fields['DELIVERY'] = static::GetFranchCityDeliveryInfo($ar_fields['PROPERTY_DELIVERY_VALUE']);
                $globalCities[ $ar_result['XML_ID'] ]['FRANCH'][ $ar_fields['ID'] ] = $ar_fields;
            }
        }

        return $globalCities;
    }
*/
	private static function cmp($a, $b)
	{
		return strcmp($a["NAME"], $b["NAME"]);
	}
}