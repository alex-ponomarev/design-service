<?php

namespace App\Http\Requests;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class PutRecordRequest

{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var DateTime
     */
    protected DateTime $dateReceipt;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var int
     */
    protected int $sort;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var int
     */
    protected int $region;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var boolean
     */
    protected bool $active;
    /**
     * @var int
     * @OA\Property(readOnly="true")
     */
    protected int $authorChange;
    /**
     * @var DateTime
     * @OA\Property(readOnly="true")
     */
    protected DateTime $dateChange;
    /**
     * @var string
     * @OA\Property(readOnly="true")
     */
    protected string $email;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @return DateTime
     */
    public function getDateReceipt(): DateTime
    {
        return $this->dateReceipt;
    }

    /**
     * @param DateTime $dateReceipt
     */
    public function setDateReceipt(DateTime $dateReceipt): void
    {
        $this->dateReceipt = $dateReceipt;
    }

    /**
     * @return int
     */
    public function getRegion(): int
    {
        return $this->region;
    }

    /**
     * @param int $region
     */
    public function setRegion(int $region): void
    {
        $this->region = $region;
    }

    /**
     * @return int
     */
    public function getSort(): ?int
    {
        return $this->sort;
    }
    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;

    }
    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
    public function getAuthorChange(): ?int
    {
        return $this->authorChange;
    }

    public function setAuthorChange(int $authorChange): self
    {
        $this->authorChange = $authorChange;

        return $this;
    }
    public function getDateChange()
    {
        return $this->dateChange;
    }

    public function setDateChange($dateChange): self
    {
        $this->dateChange = $dateChange;

        return $this;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
}