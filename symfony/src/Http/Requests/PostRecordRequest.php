<?php

namespace App\Http\Requests;

use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use OpenApi\Annotations as OA;

class PostRecordRequest

{
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(min="1", max="2048")
     *
     * @var DateTime
     */
    protected DateTime $dateReceipt;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     *
     * @var int
     */
    protected int $sort = 5;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var int
     */
    protected int $region;
    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @var boolean
     */
    protected bool $active;
    /**
     * @var int
     * @OA\Property(readOnly="true")
     */
    protected int $authorCreate;
    /**
     * @var DateTime
     * @OA\Property(readOnly="true")
     */
    protected DateTime $dateCreate;
    /**
     * @var string
     * @OA\Property(readOnly="true")
     */
    protected string $email;

    /**
     * @return DateTime
     */
    public function getDateReceipt(): DateTime
    {
        return $this->dateReceipt;
    }

    /**
     * @param DateTime $dateReceipt
     */
    public function setDateReceipt(DateTime $dateReceipt): void
    {
        $this->dateReceipt = $dateReceipt;
    }

    /**
     * @return int
     */
    public function getRegion(): int
    {
        return $this->region;
    }

    /**
     * @param int $region
     */
    public function setRegion(int $region): void
    {
        $this->region = $region;
    }
    /**
     * @param int $sort
     */
    public function setSort(int $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    public function getAuthorCreate(): ?int
    {
        return $this->authorCreate;
    }

    public function setAuthorCreate(int $authorCreate): self
    {
        $this->authorCreate = $authorCreate;

        return $this;
    }
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    public function setDateCreate($dateCreate): self
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }
    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}